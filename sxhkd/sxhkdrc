##############################################################################
# Session management
##############################################################################

alt + shift + Delete
	bspc quit && pkill lemonbar
alt + Delete
	systemctl suspend && physlock -m

##############################################################################
# Configuration
##############################################################################

# make sxhkd reload its configuration files:
alt + w
	pkill -USR1 -x sxhkd

##############################################################################
# Window management
##############################################################################

# focus or send to the given desktop
alt + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

# kill focussed window
{alt,ctrl} + q
	bspc node -c

# change current node, or swap it
alt + {_,shift + }{j,k,l,semicolon}
	bspc node --{focus,swap} {west,south,north,east}

# rotate current workspace
alt + {_,shift + }r
	bspc node @/ --rotate {90,-90}

# make current node fullscreen or monocle
alt + {_,shift + }f
	{bspc desktop -l next,bspc node --state \~fullscreen}

# switch to pseudo_tiled mode
alt + {_,shift + }s
	bspc node -t {pseudo_tiled,tiled}

# swap current node with biggest node
alt + {_,shift + }e
 	bspc node --{swap,focus} biggest

# focus the next/previous node in the current desktop
#alt + {_,shift + }a
#	bspc node -f {next,prev}.local

# focus the last node/desktop (`~ = grave key)
#alt + {_,shift +} {Tab,grave}
#	bspc {desktop,node} -f {next,prev}.local

# preselect the direction
alt + ctrl + {j,k,l,semicolon}
	bspc node -p {west,south,north,east}

# cancel the preselection for the focused node
alt + ctrl + space
	bspc node -p cancel

# set the node flags
#mod4 + {m,x,y,p}
#	bspc node -g {marked,locked,sticky,private}

##############################################################################
# User applications 
##############################################################################

#Print

{mod4, alt} + Return
	urxvt -bg 0 -fg white 
alt + g
	transmission-gtk
alt + n
	urxvt -bg 0 -e newsboat
alt + d
	~/.config/rofi/menu/search
alt + t
	tor-browser-en
alt + b
	firefox
alt + y
	signal-desktop
alt + u
	bash ~/bas/mus.sh
alt + i
	sxiv -t ~/pic/*/*/*
alt + o
	nautilus
alt + p
	keepmenu
alt + v
	~/.scripts/string_handler.sh -i
alt + c
	~/.scripts/string_handler.sh -p

##############################################################################
# Media management
##############################################################################

alt + Left
	xdotool search --class mpv |xargs -I % xdotool key --window % shift+60
alt + Right
	xdotool search --class mpv |xargs -I % xdotool key --window % shift+94
alt + Up
	xdotool search --class mpv |xargs -I % xdotool key --window % space
#alt + Down
#	xdotool search --class mpv |xargs -I % xdotool key --window % q

##############################################################################
# Special keys
##############################################################################

mod1 + Print
	~/.scripts/printscreen.sh
XF86Display
	xrandr --output HDMI-2 --mode 1920x1080

XF86Tools
	urxvt -bg 0 -e calcurse

XF86Favorites
	bash ~/bas/mus.sh

{XF86AudioLowerVolume,XF86AudioRaiseVolume}
	{amixer -q sset Master 1%-,amixer -q sset Master 1%+}

XF86AudioMute
	amixer -D pulse set Master 1+ toggle 

{XF86MonBrightnessUp,XF86MonBrightnessDown}
	{bash ~/bas/backlight.sh -i 2,	bash ~/bas/backlight.sh -i -2}

shift + {XF86MonBrightnessUp,XF86MonBrightnessDown}
	{bash ~/bas/backlight.sh -i 10,	bash ~/bas/backlight.sh -i -10}

XF86AudioPrev
	xdotool search --class mpv |\
	xargs -I % xdotool key --window % shift+60
XF86AudioNext
	xdotool search --class mpv |\
	xargs -I % xdotool key --window % shift+94

XF86AudioPlay
	xdotool search --class mpv | xargs -I % xdotool key --window % space

##############################################################################
# Cursor bindings 
##############################################################################

bspc config pointer_mod         mod4
bspc config pointer_action1     move

~button1
	~/.scripts/wm/DidIClickDesktop.sh && \
	~/.scripts/wm/CursorWorkspaceSwitcher.sh

~button3
	~/.scripts/wm/DidIClickDesktop.sh && \
	~/.scripts/wm/CursorWorkspaceSender.sh \
	~/.scripts/wm/CursorWorkspaceSwitcher.sh
~button2
	~/.scripts/wm/DidIClickDesktop.sh && bspc node -c
