#!/bin/bash
# set theme

option="$1"
wal_image="$2"

case $option in
	-d) 	echo "Setting up dark theme"
		wal -ti "$wal_image" -b 000000
#		notify-send "theme updated"
	;;
	*)	echo -e "Script to set a theme for rice:"
		echo -e "\tfirst parameter options:  -d [set theme]"
		echo -e "\tSecond parameter should be the wallpaper."
	;;
esac

