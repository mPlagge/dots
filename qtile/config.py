#!/usr/bin/env python

'''Qtile config file'''

from libqtile.config import Key, Screen, Group, Match, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import Spacer, base

keys = []
mod = 'mod1'
workspaces_string = '1234567890'
#workspaces_string = 'asdfuiop'

layout_config = dict(border_width=0, margin=5)
layouts = [
    #layout.Columns(**layout_config),
    #layout.RatioTile(**layout_config), # decent
    #layout.Zoomy(**layout_config), # BAD
    #layout.Matrix(**layout_config), # Decent
    layout.MonadTall(**layout_config), # Decent
]

# Add keys for changing and sending windows workspaces 
groups = [Group(i) for i in workspaces_string]
for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod, 'shift'], i.name, lazy.window.togroup(i.name))
    ])

APPS = {
    'my_terminal': 'urxvt',
    'my_web_browser_main': 'firefox',
    # comparmentalised browser when using unprivate services like google services, outlook, etc
    'my_web_browser_unprivate': 'brave',    
    # browser for comparmentalised private browsing
    'my_web_browser_private': 'firefox',
    'my_torrent_client': 'transmission-gtk',
    'my_music_player': '/home/m/.scripts/mus.sh',
    'my_pw_manager': 'keepmenu',
    'my_wallpaper_chooser': '/home/m/.scripts/wallpapers.sh',
    'my_application_menu': '/home/m/.config/rofi/menu/search',
    'my_rss_viewer': 'urxvt -bg 0 -e newsboat',
    'my_screenshot': '/home/m/.scripts/screenshot.sh',
}

ACTIONS = {
    'backlight_up': '/home/m/.scripts/backlight.sh -i 10',
    'backlight_down': '/home/m/.scripts/backlight.sh -i -10',
    'hdmi_connect': 'xrandr --output HDMI-2 --mode 1920x1080',
    'volume_mute': 'amixer -D pulse set Master 1+ toggle',
    'volume_up': 'amixer -q sset Master 1%+',
    'volume_down': 'amixer -q sset Master 1%-',
}

keys.extend([
    # Applications/Scripts Shortcuts
    Key([mod], 'Return', lazy.spawn(APPS['my_terminal'])),
    Key([mod], 'b', lazy.spawn(APPS['my_web_browser_main'])),
    Key([mod], 't', lazy.spawn(APPS['my_web_browser_private'])),
    Key([mod], 'g', lazy.spawn(APPS['my_torrent_client'])),
    Key([mod], 'u', lazy.spawn(APPS['my_music_player'])),
    Key([mod], 'p', lazy.spawn(APPS['my_pw_manager'])),
    Key([mod], 'i', lazy.spawn(APPS['my_wallpaper_chooser'])),
    Key([mod], 'd', lazy.spawn(APPS['my_application_menu'])),
    Key([mod], 'n', lazy.spawn(APPS['my_rss_viewer'])),
    Key([mod], 'o', lazy.spawn(APPS['my_screenshot'])),
    # Session
    Key([mod, 'shift'], 'q', lazy.shutdown()),
    Key([mod, 'shift'], 'r', lazy.restart()),
    # Layouts
    # Key([mod], 'r', lazy.group.setlayout('max'),
    # general window managements
    Key([mod], 'q', lazy.window.kill()),
    Key([mod], 'f', lazy.window.toggle_fullscreen()),
    Key([mod], 'space', lazy.window.toggle_fullscreen()),
    # Mirror Workspace
    Key([mod, 'shift'], 'space', lazy.layout.flip()),
    # Change Focus
    Key([mod], 'h', lazy.layout.left()),
    Key([mod], 'l', lazy.layout.right()),
    Key([mod], 'j', lazy.layout.down()),
    Key([mod], 'k', lazy.layout.up()),
    # Swap Windows
    Key([mod, 'shift'], 'h', lazy.layout.swap_left()),
    Key([mod, 'shift'], 'l', lazy.layout.swap_right()),
    Key([mod, 'shift'], 'j', lazy.layout.shuffle_down()),
    Key([mod, 'shift'], 'k', lazy.layout.shuffle_up()),
])

widget_defaults = dict(
    font='monospace',
    fontsize=12,
    padding=3,
    background='ffffff',
    foreground='000000',
)
extension_defaults = widget_defaults.copy()

screens = []
            
# Drag floating layouts.
mouse = [
    Drag([mod], 'Button1', lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], 'Button3', lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], 'Button2', lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'Tor Browser'},
])
auto_fullscreen = True
focus_on_window_activation = 'smart'

wmname = 'LG3D'
